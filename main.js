// 1

let strings = ["travel", "hello", "eat", "ski", "lift"];
let count = strings.filter(str => str.length > 3).length;
console.log(count); 

// 2
let people = [   { name: "Іван", age: 25, sex: "чоловіча" },   { name: "Марія", age: 30, sex: "жіноча" },   { name: "Петро", age: 40, sex: "чоловіча" },   { name: "Ольга", age: 35, sex: "жіноча" } ];
let filteredPeople = people.filter(person => person.sex === "чоловіча");
console.log(filteredPeople); 


// 3

function filterBy(arr, dataType) {     
return arr.filter(item => typeof item !== dataType);
} 

let data = ['hello', 'world', 23, '23', null]; 
let filteredData = filterBy(data, 'string'); 
console.log(filteredData); 

